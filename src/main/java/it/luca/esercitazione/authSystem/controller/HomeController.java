package it.luca.esercitazione.authSystem.controller;

import it.luca.esercitazione.authSystem.entity.requestModel.AuthenticationRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HomeController {

    @GetMapping("/")
    public String home(){
        return ("<h1>Welcome<h1>");
    }

    @GetMapping("/user")
    public String user(){
        return ("<h1>Welcome, user<h1>");
    }

    @GetMapping("/admin")
    public String admin(){
        return ("<h1>Welcome, admin<h1>");
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{


    }

}
