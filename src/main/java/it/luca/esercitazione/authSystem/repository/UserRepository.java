package it.luca.esercitazione.authSystem.repository;

import it.luca.esercitazione.authSystem.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,Integer> {

    public Optional<User> findByUsername(String username);

}
