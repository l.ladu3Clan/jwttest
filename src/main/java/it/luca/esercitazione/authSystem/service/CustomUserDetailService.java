package it.luca.esercitazione.authSystem.service;

import it.luca.esercitazione.authSystem.entity.CustomUserDetails;
import it.luca.esercitazione.authSystem.entity.User;
import it.luca.esercitazione.authSystem.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found :"+ username));

        return user.map(CustomUserDetails::new).get();

    }
}
